import logging

import pytest
from _pytest.logging import LogCaptureFixture

import rpm_diff


def test_nvr() -> bool:
    assert type(rpm_diff.nvr("libsmbclient-4.14.5-2.el8")) is tuple


def test_nvr_failed(caplog: LogCaptureFixture) -> bool:
    caplog.set_level(logging.ERROR)
    rpm_diff.nvr("notAnNvrPattern")
    assert "object has no attribute" in caplog.text


@pytest.mark.parametrize(
    "rpm1, rpm2, expected",
    [
        (
            "libsmbclient-4.14.5-2.el8",
            "libsmbclient-4.15.5-2.el8",
            "libsmbclient-4.15.5-2.el8",
        ),
        (
            "libsmbclient-4.16.5-2.el8",
            "libsmbclient-4.15.5-2.el8",
            "libsmbclient-4.16.5-2.el8",
        ),
        (
            "libsmbclient-4.16.5-2.el8",
            "libsmbclient-4.16.5-2.el8",
            "libsmbclient-4.16.5-2.el8",
        ),
        (
            "NetworkManager-1.32.3-0.z.3.d8706caef2.el8",
            "ModemManager-1.50.8-3.el8",
            "",
        ),
    ],
)
def test_diff(rpm1: str, rpm2: str, expected: str) -> bool:
    assert rpm_diff.diff(rpm1, rpm2) == expected


def test_diff_failed(caplog: LogCaptureFixture) -> bool:
    caplog.set_level(logging.ERROR)
    rpm_diff.diff("unknownPattern", "unknownPatter12")
    assert "'NoneType' object has no attribute 'groups'" in caplog.text


@pytest.mark.parametrize(
    "e1, v1, r1, e2, v2, r2, expected",
    [
        ("libsmbclient", "4.14.5", "el8", "libsmbclient", "4.15.5", "el8", -1),
        ("libsmbclient", "4.16.5", "el8", "libsmbclient", "4.15.5", "el8", 1),
        ("libsmbclient", "4.16.5", "el8", "libsmbclient", "4.16.5", "el8", 0),
        (
            "NetworkManager",
            "1.32.3",
            "0.z.3.d8706caef2.el8",
            "ModemManager",
            "1.50.8",
            "el8",
            -3,
        ),
    ],
)
def test_vercmp(
    e1: str, v1: str, r1: str, e2: str, v2: str, r2: str, expected: int
) -> bool:
    assert rpm_diff.vercmp(e1, v1, r1, e2, v2, r2) == expected


@pytest.mark.parametrize(
    "e1, v1, r1, e2, v2, r2, expected",
    [
        ("libsmbclient", 4.14, "el8", "libsmbclient", "4.15.5", "el8", -2),
        ("libsmbclient", "4.16.5", "el8", "libsmbclient", 4.15, "el8", -2),
        ("libsmbclient", 4, "el8", "libsmbclient", 5, "el8", -2),
        ("NetworkManager", "1.32.3", 5, "NetworkManager", "1.50.8", "el8", -2),
    ],
)
def test_vercmp_failed(
    e1: str, v1: str, r1: str, e2: str, v2: str, r2: str, expected: int
) -> bool:
    assert rpm_diff.vercmp(e1, v1, r1, e2, v2, r2) == expected
