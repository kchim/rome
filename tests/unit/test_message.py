import json
import logging
from unittest import mock

import requests
import requests_mock
import responses
from _pytest.logging import LogCaptureFixture

import message
from message import Message


def test_fetch_upstream_manifest() -> bool:
    with requests_mock.mock() as m:
        m.get(
            "https://gitlab.com/redhat/automotive/automotive-sig/-/raw/"
            "main/package_list/cs9-image-manifest.lock.json",
            text='["cs9", {"common":["libsmbclient-4.14.5-2.el8", \
            "ModemManager-1.10.8-3.el8"]}]',
        )
        assert message.get_upstream_manifest("cs9_manifest_url") == [
            "cs9",
            {
                "common": [
                    "libsmbclient-4.14.5-2.el8",
                    "ModemManager-1.10.8-3.el8",
                ]
            },
        ]


def test_fetch_upstream_manifest_with_exception(
    caplog: LogCaptureFixture,
) -> bool:
    caplog.set_level(logging.ERROR)
    with mock.patch("requests.get") as requests_get:
        requests_get.side_effect = requests.exceptions.RequestException
        value = message.get_upstream_manifest("cs9_manifest_url")
    assert value == "" and "RequestException" in caplog.text


@responses.activate
def test_submit_package(submit_package_vars: list, nvrs: list) -> bool:
    responses.add(
        responses.POST,
        url="http://dover:8080/submit/package_list",
        body="Package sent",
    )
    assert (
        message.submit_package(
            submit_package_vars[2][0],
            submit_package_vars[1],
            ",".join(nvrs),
            submit_package_vars[2],
            "cs9-image-manifest.lock.json",
        )
        == 200
    )


def test_submit_package_failed(
    caplog: LogCaptureFixture, submit_package_vars: list, nvrs: list
) -> bool:
    caplog.set_level(logging.ERROR)
    with mock.patch("requests.post") as requests_post:
        requests_post.side_effect = requests.exceptions.HTTPError
        value = message.submit_package(
            submit_package_vars[2][0],
            submit_package_vars[1],
            ",".join(nvrs),
            submit_package_vars[2],
            "cs9-image-manifest.lock.json",
        )
    assert value == 0 and "HTTPError" in caplog.text


def test_set_package_schema():
    test_obj = Message()
    assert len(test_obj.pkg_schema_obj.common) == 0
    test_obj.set_package_schema("cs9_manifest_url")
    assert len(test_obj.pkg_schema_obj.common) > 0


def test_parse_and_diff_success(json_file_data: str):
    test_obj = Message()
    test_obj.upstream_nvr = "abattis-cantarell-fonts-1.0.25-6.el8"
    test_obj.pkg_schema_obj.deserialize(json.loads(json_file_data))
    result = test_obj.parse_and_diff()
    assert result == 1


def test_parse_and_diff_failure(json_file_data: str):
    test_obj = Message()
    test_obj.upstream_nvr = "abattis-cantarell-fonts-0.0.25-6.el8"
    test_obj.pkg_schema_obj.deserialize(json.loads(json_file_data))
    result = test_obj.parse_and_diff()
    assert result == 0


def test_call_diff_failure_with_partial_non_matching_nvr():
    nvr_list: list = ["libsmbclient-helper-4.14.5-2.el8"]
    test_obj = Message()
    test_obj.upstream_nvr = "libsmbclient-4.14.5-2.el8"
    result, nvr_list = test_obj.call_diff(nvr_list)
    assert result == 0, nvr_list[0] == "libsmbclient-helper-4.14.5-2.el8"


def test_call_diff_failure_with_same_nvr():
    nvr_list: list = ["libsmbclient-4.14.5-2.el8"]
    test_obj = Message()
    test_obj.upstream_nvr = "libsmbclient-4.14.5-2.el8"
    result, nvr_list = test_obj.call_diff(nvr_list)
    assert result == 0, nvr_list[0] == "libsmbclient-4.14.5-2.el8"


def test_call_diff_success():
    nvr_list: list = ["libsmbclient-4.14.5-2.el8"]
    test_obj = Message()
    test_obj.upstream_nvr = "libsmbclient-5.14.5-2.el8"
    result, nvr_list = test_obj.call_diff(nvr_list)
    assert result == 1, nvr_list[0] == "libsmbclient-5.14.5-2.el8"


@responses.activate
def test_send_message(
    submit_package_vars: list, json_file_data: str, message_body: str
):
    responses.add(
        responses.POST,
        url="http://dover:8080/submit/package_list",
        body="Package sent",
    )
    assert (
        message.submit_package(
            submit_package_vars[2][0],
            submit_package_vars[1],
            json_file_data,
            submit_package_vars[2],
            "cs9-image-manifest.lock.json",
        )
        == 200
    )
