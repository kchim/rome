import logging
import re

import rpm


def nvr(rpm: str) -> tuple:
    pattern1 = re.compile(
        r"^([a-zA-Z0-9_\-\+]*)-([a-zA-Z0-9_\.]*)-([a-zA-Z0-9_\.]*)"
    )
    result = ()
    try:
        result = pattern1.search(rpm).groups()
    except (AttributeError, ValueError) as e:
        logging.exception(e)

    return result


def vercmp(e1: str, v1: str, r1: str, e2: str, v2: str, r2: str) -> int:
    if e1 != e2:
        return -3

    try:
        return rpm.labelCompare((e1, v1, r1), (e2, v2, r2))
    except ValueError as e:
        logging.exception(e)
        return -2
    except TypeError as e:
        logging.exception(e)
        return -2


def diff(rpm1: str, rpm2: str) -> str:
    rc = -10
    try:
        (e1, v1, r1) = nvr(rpm1)
        (e2, v2, r2) = nvr(rpm2)
    except (AttributeError, ValueError) as e:
        logging.exception(e)

    try:
        rc = vercmp(e1, v1, r1, e2, v2, r2)
    except Exception as e:
        logging.exception(e)

    if rc == 1:
        logging.info(f"{e1}:{v1}-{r1} is newer")
        return rpm1
    elif rc == 0:
        logging.info(f"These are equal: {rpm1} and {rpm2}")
        return rpm1
    elif rc == -1:
        logging.info(f"{e2}:{v2}-{r2} is newer")
        return rpm2
    elif rc == -3:
        logging.info("Rpms are not matched")
        return ""
    else:
        logging.exception("Exception occured!")
        return ""
