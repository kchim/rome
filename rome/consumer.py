import logging

from fedora_messaging import api

import local_conf
from message import format_message

logger = logging.getLogger("rome.Consumer")


def printer_callback(message):
    """
    Print the message to standard output.

    Args:
        message (fedora_messaging.message.Message): The message we received
            from the queue.
    """

    return_msg = format_message(message)
    if return_msg["status"] == "":
        logger.info("Message received: Package is not relevant.")
    elif return_msg["status"] == 200:
        logger.info("Message received: MR opened!")
    else:
        logger.error(f"MR not raised with status code {return_msg['status']}")


def consume_message():
    api.consume(
        printer_callback,
        bindings=local_conf.BINDINGS,
        queues=local_conf.QUEUES,
    )
