UUID_1 = "54d0a4e4-06bf-4030-a6dd-28229a3f553f"

UUID_PROPERTIES = {
    "durable": False,
    "auto_delete": True,
    "exclusive": False,
    "arguments": {},
}

QUEUES = {
    UUID_1: UUID_PROPERTIES,
}

BINDINGS = [
    {
        "exchange": "amq.topic",
        "queue": UUID_1,
        "routing_keys": [
            "org.centos.prod.buildsys.tag",
        ],
    },
]
