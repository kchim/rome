#!/bin/bash

set -e

checkoutdir=$(pwd)

yoctorequires="gawk make wget tar bzip2 gzip python3 unzip perl patch diffutils diffstat git cpp gcc gcc-c++ glibc-devel texinfo chrpath ccache perl-Data-Dumper perl-Text-ParseWords perl-Thread-Queue perl-bignum socat python3-pexpect findutils which file cpio python python3-pip xz python3-GitPython python3-jinja2 SDL-devel xterm rpcgen mesa-libGL-devel make python3-pip which python3-sphinx python3-sphinx_rtd_theme python3-pyyaml ccache"
metarpmrequires="patchelf shadow-utils"

## tested on Fedora 33

echo "Installing Yocto BaseOS requirements"

# https://docs.yoctoproject.org/ref-manual/system-requirements.html#required-packages-for-the-build-host
# note: no need to pip install sphinx, sphinx_rtd_theme or pyyaml.

sudo dnf install -y $yoctorequires

echo "Installing meta-rpm BaseOS requirements"

sudo dnf install -y $metarpmrequires

if [ ! -d meta-rpm ]; then
	echo "Cloning meta-rpm"
	git clone https://gitlab.com/fedora-iot/meta-rpm.git
	cd meta-rpm
	git submodule update --init
else
	echo "Updating meta-rpm git tree"
	cd meta-rpm
	git pull
	git submodule update --recursive
fi

echo "Building meta-rpm base images. This can take a very long time!"

images=$(ls -1 meta-rpm/recipes-core/images/ | sed -e 's#.bb##g')

# need to disable shell errors here
set +e
source oe-init-build-env
set -e

for i in $images; do
	echo "Building $i"
	bitbake $i
done

# .manifest files contain the list of installed rpms in the images

imagerpms="$(cat tmp/deploy/images/qemux86-64/*.manifest | awk '{print $1}' | sort -u)"

allrpms="$yoctorequires $metarpmrequires $imagerpms"

cd $checkoutdir >/dev/null 2>&1

echo "Installing content-resolver requirements"

sudo dnf install -y python3-yaml python3-jinja2

if [ ! -d content-resolver ]; then
	echo "Cloning content-resolver"
	git clone https://github.com/minimization/content-resolver.git
	cd content-resolver
else
	cd content-resolver
	git pull
fi

# this is not working yet
mkdir -p a-team
mkdir -p output/history

./feedback_pipeline.py a-team output
