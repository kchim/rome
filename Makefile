.PHONY: dependencies unit lint image/build up up/ci ci/stage/up ci/prod/up tools/image/build stage/up prod/up dev/up

ifneq (, $(shell which podman))
CONTAINER_ENGINE := podman
else
CONTAINER_ENGINE := docker
endif
TOOLS_IMAGE=rome-tools
in_container = ${CONTAINER_ENGINE} run --rm -it ${TOOLS_IMAGE} make env=$1 $2

unit:
	PYTHONPATH="$PYTHONPATH:./rome" poetry run pytest --cov=rome --cov-report \
		term-missing --cov-fail-under=65 tests/unit

lint:
	poetry run isort .
	poetry run black rome tests
	poetry run flake8 rome tests

up:
	ansible-playbook deployment/rome-main.yml -e env=$(env)

up/ci:
	ansible-playbook deployment/ci-main.yml -e env=$(env)

image/build:
	${CONTAINER_ENGINE} build -t rome .

tools/image/build:
	${CONTAINER_ENGINE} build -t ${TOOLS_IMAGE} -f deployment/Dockerfile.tools .

ci/stage/up: tools/image/build
	$(call in_container,stage,up/ci)

ci/prod/up: tools/image/build
	$(call in_container,stage,up/ci)

dev/up: image/build
	${CONTAINER_ENGINE} run -it --network=host -e FEDORA_MESSAGING_CONF=conf.toml rome

stage/up: tools/image/build
	$(call in_container,stage,up)

prod/up: tools/image/build
	$(call in_container,prod,up)
